import os.path
from PIL import Image

# Path to folder with images
SOURCE_FOLDER_PATH = "D:\hubble_dataset"
SOURCE_FOLDER_PATH = os.path.abspath(SOURCE_FOLDER_PATH)

# Image size
HEIGHT = 256
WIDTH = 256

# Image save format
FORMAT = "jpg"

# Dataset folder name
DATASET_FOLDER_NAME = SOURCE_FOLDER_PATH.split(os.sep)[-1] + "_" + str(WIDTH) + "x" + str(HEIGHT) + "_" + FORMAT

counter = 0


if not os.path.exists(DATASET_FOLDER_NAME):
    os.mkdir(DATASET_FOLDER_NAME)

for file in os.listdir(SOURCE_FOLDER_PATH):
    file_path = os.path.join(SOURCE_FOLDER_PATH, file)
    file_ext = file.split('.')[-1]
    if os.path.isfile(file_path) and file_ext in ("jpg", "png"):
        save_file_name = str(counter) + "." + FORMAT
        save_file_path = os.path.join(DATASET_FOLDER_NAME, save_file_name)

        with Image.open(file_path) as im:

            if im.mode == "RGBA":
                im = im.convert('RGB')

            im = im.resize((WIDTH, HEIGHT))
            im.save(save_file_path)

        counter += 1
